Role Name
=========

Install a minimal Python environment on a system to enable Ansible
to be used to manage it.

Requirements
------------

None

Role Variables
--------------

**bootstrap_os** - Specify the flavor of OS of the target system. 
One of ("ubuntu", "debian", "coreos", "centos")

**install_pip** - if `yes`, will install the Python pip utility. (Centos only for now)

**proxy_url** - *Optional* Configure a proxy for the packaging system at the given URL.
(Centos only for now)

**install_epel** - if `yes`, will install the epel repo (Centos only)


Dependencies
------------

None

Example Playbook
----------------
```
- hosts: servers
  gather_facts: no
  become: yes

  include_role:
    - bootstrap_ansible
  vars:
    bootstrap_os: "ubuntu"
```
License
-------

Apache

Author Information
------------------
Modified by Paul T Sparks from github.com/kubernetes-incubator/kubespray. 
